import 'package:flutter/cupertino.dart';
import 'package:glasess_app/models/category_model.dart';

class ItemModel {
  int id;
  String name;
  String icon;
  CategoryModel category;
  double price;
  double ratings;
  int reviewed;
  Color color;

  ItemModel(this.id, this.name, this.icon, this.category, this.price,
      this.ratings, this.reviewed, this.color);
}
