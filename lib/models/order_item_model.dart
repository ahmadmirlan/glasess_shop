import 'package:glasess_app/models/item_model.dart';

class OrderedItemModel {
  late ItemModel item;
  late int quantity;

  OrderedItemModel(this.item, this.quantity);
}
