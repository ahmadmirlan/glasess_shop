import 'package:glasess_app/models/order_item_model.dart';

class CountTotalHelper {
  static double countSubTotal(List<OrderedItemModel> orderedItem) {
    var countTotal = 0.0;
    for (var element in orderedItem) {
      final total = element.quantity * element.item.price;
      countTotal = countTotal + total;
    }
    return countTotal;
  }

  static double countPPN(double total) {
    return total * 0.11;
  }
}
