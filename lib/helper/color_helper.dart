import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color whiteDarker = const Color.fromRGBO(246, 246, 246, 1);
  static Color primary = const Color.fromRGBO(255, 185, 2, 1);
  static Color secondary = const Color.fromRGBO(251, 231, 232, 1);
  static Color pink = Colors.pinkAccent;
  static Color brown = Colors.brown;
  static Color blue = Colors.lightBlue;
  static Color dark = const Color.fromRGBO(57, 57, 57, 1);
  static Color lightDark = const Color.fromRGBO(19, 33, 46, 1);
  static Color lightDarkSecondary = const Color.fromRGBO(27, 42, 63, 1);
  static Color grey = const Color.fromRGBO(246, 246, 248, 1);
  static Color yellow = const Color.fromRGBO(237, 179, 82, 1);
  static Color orange = const Color.fromRGBO(251, 162, 68, 1);
}
