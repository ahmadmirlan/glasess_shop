import 'package:get/get.dart';
import 'package:glasess_app/models/item_model.dart';
import 'package:glasess_app/models/order_item_model.dart';

class CartController extends GetxController {
  List<OrderedItemModel> _cartList = [];

  List<OrderedItemModel> get cartList => _cartList;

  void addItemToCart(ItemModel item) {
    OrderedItemModel? isExist = _cartList.firstWhereOrNull((element) {
      return element.item.id == item.id;
    });

    if (isExist == null) {
      _cartList.add(OrderedItemModel(item, 1));
      update();
    }
  }

  void addItemQuantity(ItemModel item) {
    var currentOrder = _cartList.indexWhere((element) {
      return element.item.id == item.id;
    });

    if (currentOrder > -1) {
      _cartList[currentOrder].quantity += 1;
      update();
      return;
    }
    _cartList.add(OrderedItemModel(item, 1));
    update();
  }

  void removeItemQuantity(ItemModel item) {
    var currentOrder = _cartList.indexWhere((element) {
      return element.item.id == item.id;
    });

    if (currentOrder < 0) {
      return;
    }

    if (_cartList[currentOrder].quantity < 2) {
      _cartList.removeAt(currentOrder);
      update();
      return;
    }

    _cartList[currentOrder].quantity -= 1;
    update();
  }

  void resetCart() {
    _cartList = [];
    update();
  }
}
