import 'package:get/get.dart';
import 'package:glasess_app/data/dummy_data.dart';
import 'package:glasess_app/models/category_model.dart';
import 'package:glasess_app/models/item_model.dart';

class ItemController extends GetxController {
  List<CategoryModel> _allCategories = [];
  late CategoryModel _selectedCategory;
  List<ItemModel> _allItems = [];
  late ItemModel _selectedItem;

  List<CategoryModel> get allCategories => _allCategories;

  CategoryModel get selectedCategory => _selectedCategory;

  List<ItemModel> get allItems => _allItems;

  ItemModel get selectedItem => _selectedItem;

  @override
  void onInit() {
    super.onInit();
    initMainData();
  }

  void initMainData() {
    _allCategories = DummyData.categories;
    _selectedCategory = _allCategories[0];
    _allItems = DummyData.items;
    update();
  }

  void changeCategory(CategoryModel category) {
    _selectedCategory = category;
    update();
  }

  void changeSelectedItem(ItemModel item) {
    _selectedItem = item;
  }
}
