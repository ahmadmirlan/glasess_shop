import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/default_loading_widget.dart';
import 'package:glasess_app/components/shared/filter_widget.dart';
import 'package:glasess_app/components/shared/item_card_widget.dart';
import 'package:glasess_app/components/shared/search_widget.dart';
import 'package:glasess_app/controller/item_controller.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/routes/AppRoutes.dart';
import 'package:loader_overlay/loader_overlay.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    final screenHeight = Get.height;
    return GetBuilder<ItemController>(builder: (controller) {
      return Column(
        children: [
          Container(
            margin: EdgeInsets.only(
                top: screenHeight * 0.03, bottom: screenHeight * 0.03),
            padding: EdgeInsets.only(
                left: screenWidth * 0.05, right: screenWidth * 0.05),
            child: const SearchWidget(),
          ),
          SizedBox(
            height: 60,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  padding: EdgeInsets.only(right: screenWidth * 0.05),
                );
              },
              padding: EdgeInsets.only(
                  left: screenWidth * 0.05, right: screenWidth * 0.05),
              itemCount: controller.allCategories.length,
              itemBuilder: (ctx, index) {
                return GestureDetector(
                    onTap: () {
                      controller
                          .changeCategory(controller.allCategories[index]);
                    },
                    child: FilterWidget(
                        category: controller.allCategories[index],
                        isSelected: controller.selectedCategory.number ==
                            controller.allCategories[index].number));
              },
            ),
          ),
          Expanded(
            child: Container(
              color: ColorHelper.whiteDarker,
              child: GridView.count(
                  padding: EdgeInsets.only(
                      left: screenWidth * 0.06,
                      right: screenWidth * 0.06,
                      top: screenHeight * 0.02,
                      bottom: screenHeight * 0.02),
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                  childAspectRatio: 0.7,
                  physics: const BouncingScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics()),
                  children: List.generate(controller.allItems.length, (index) {
                    return GestureDetector(
                      onTap: () {
                        controller
                            .changeSelectedItem(controller.allItems[index]);
                        context.loaderOverlay
                            .show(widget: const DefaultLoadingWidget());
                        Future.delayed(const Duration(seconds: 1), () {
                          Get.toNamed(AppRoutes.ITEM_DETAIL);
                          context.loaderOverlay.hide();
                        });
                      },
                      child: ItemCardWidget(item: controller.allItems[index]),
                    );
                  })),
            ),
          )
        ],
      );
    });
  }
}
