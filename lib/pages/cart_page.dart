import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/cart_item_card_widget.dart';
import 'package:glasess_app/components/shared/cart_summary_widget.dart';
import 'package:glasess_app/controller/cart_controller.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(builder: (controller) {
      return Column(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: Get.height * 0.02, bottom: Get.height * 0.02),
                child: ListView.separated(
                  physics: const BouncingScrollPhysics(),
                    padding: EdgeInsets.only(
                        left: Get.width * 0.05, right: Get.width * 0.05),
                    itemBuilder: (context, index) {
                      return CartItemCardWidget(
                          orderedItem: controller.cartList[index]);
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: EdgeInsets.only(bottom: Get.height * 0.03),
                      );
                    },
                    itemCount: controller.cartList.length)),
          ),
          Container(
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            margin: EdgeInsets.only(bottom: Get.height * 0.05),
            child: CartSummaryWidget(orderedItem: controller.cartList),
          )
        ],
      );
    });
  }
}
