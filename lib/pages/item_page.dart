import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/description_widget.dart';
import 'package:glasess_app/controller/item_controller.dart';
import 'package:glasess_app/data/dummy_data.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/models/item_model.dart';

class ItemPage extends StatelessWidget {
  const ItemPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ItemController>();
    final ItemModel item = controller.selectedItem;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        Container(
          height: Get.height * 0.3,
          decoration: BoxDecoration(
              color: item.color,
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(35),
                  bottomRight: Radius.circular(35))),
          child: Image.asset('assets/images/${item.icon}'),
        ),
        Container(
          padding:
              EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
          margin: EdgeInsets.only(
              top: Get.height * 0.03, bottom: Get.height * 0.015),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: Get.width * 0.5,
                child: Text(item.name,
                    style: const TextStyle(
                        fontSize: 24, fontWeight: FontWeight.w600)),
              ),
              Text('\$${item.price.toStringAsFixed(2)}',
                  style: const TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 20))
            ],
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Icon(Icons.star_outlined, color: ColorHelper.yellow),
                  Container(
                    margin: EdgeInsets.only(left: Get.width * 0.01),
                  ),
                  Text('${item.ratings}',
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600)),
                  Container(
                    margin: EdgeInsets.only(left: Get.width * 0.03),
                  ),
                  Text('(${item.reviewed} Review)')
                ],
              ),
              Text('Available in Stocks',
                  style: TextStyle(
                      fontSize: 18, color: ColorHelper.dark.withOpacity(0.4)))
            ],
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
          margin: EdgeInsets.only(
              top: Get.height * 0.03, bottom: Get.height * 0.015),
          child: const Text('Overview',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600)),
        ),
        Container(
          padding:
              EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
          child: DescriptionWidget(text: DummyData.description),
        ),
        Container(
          padding:
              EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
          margin: EdgeInsets.only(
              top: Get.height * 0.03, bottom: Get.height * 0.015),
          child: const Text('Related Products',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600)),
        ),
        Container(
            height: 60,
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            child: ListView.separated(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    width: Get.width * 0.187,
                    height: Get.width * 0.187,
                    decoration: BoxDecoration(
                        color: controller.allItems[index].color,
                        borderRadius: BorderRadius.circular(10)),
                    child: Image.asset(
                        'assets/images/${controller.allItems[index].icon}'),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: EdgeInsets.only(right: Get.width * 0.05),
                  );
                },
                itemCount: 4)),
      ],
    );
  }
}
