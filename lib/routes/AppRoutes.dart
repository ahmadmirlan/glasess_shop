class AppRoutes {
  static const String HOME = '/';
  static const String ITEM_DETAIL = '/item-detail';
  static const String CART = '/cart';
  static const String ORDER_COMPLETE = '/completed';
}
