import 'package:get/get.dart';
import 'package:glasess_app/controller/cart_binding.dart';
import 'package:glasess_app/controller/item_binding.dart';
import 'package:glasess_app/screens/cart_screen.dart';
import 'package:glasess_app/screens/home_screen.dart';
import 'package:glasess_app/screens/item_screen.dart';
import 'package:glasess_app/screens/order_completed_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.HOME,
        page: () => const HomeScreen(),
        bindings: [ItemBinding(), CartBinding()]),
    GetPage(
        name: AppRoutes.ITEM_DETAIL,
        page: () => const ItemScreen(),
        bindings: [ItemBinding(), CartBinding()]),
    GetPage(
        name: AppRoutes.CART,
        page: () => const CartScreen(),
        binding: CartBinding()),
    GetPage(
        name: AppRoutes.ORDER_COMPLETE,
        page: () => const OrderCompletedScreen(),
        binding: CartBinding()),
  ];
}
