import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/item_footer_navigator.dart';
import 'package:glasess_app/controller/item_controller.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/pages/item_page.dart';

class ItemScreen extends StatelessWidget {
  const ItemScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ItemController>();
    return Scaffold(
      appBar: AppBar(
          elevation: 0,
          backgroundColor: controller.selectedItem.color,
          leading: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.05,
                  top: Get.width * 0.01,
                  bottom: Get.width * 0.01),
              child: Container(
                decoration: BoxDecoration(
                    color: ColorHelper.dark,
                    borderRadius: BorderRadius.circular(10)),
                child: Icon(Icons.navigate_before_sharp,
                    color: ColorHelper.white, size: 35),
              ),
            ),
          ),
          leadingWidth: Get.width * 0.17,
          centerTitle: true,
          title: const Text('Product Details',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600))),
      body: const ItemPage(),
      bottomNavigationBar: ItemFooterNavigator(item: controller.selectedItem),
    );
  }
}
