import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/menu_bar_widget.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/pages/home_page.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ColorHelper.white,
          leading: Container(
            padding: EdgeInsets.only(
                left: Get.width * 0.05,
                top: Get.width * 0.01,
                bottom: Get.width * 0.01),
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
              clipBehavior: Clip.antiAlias,
              child: Image.asset('assets/icons/avatar.jpg', fit: BoxFit.cover),
            ),
          ),
          leadingWidth: Get.width * 0.17,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Hello Mirlan,',
                  style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
              Text('Welcome back!',
                  style: TextStyle(
                      fontSize: 14,
                      color: ColorHelper.dark.withOpacity(0.5))),
            ],
          ),
          actions: [
            Container(
                width: 40,
                margin: EdgeInsets.only(right: Get.width * 0.04),
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: ColorHelper.dark)),
                child: const Icon(Icons.shopping_basket_outlined))
          ],
        ),
        body: const HomePage(),
      bottomNavigationBar: const MenuBarWidget(),
    );
  }
}
