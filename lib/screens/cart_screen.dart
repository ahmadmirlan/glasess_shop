import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/default_loading_widget.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/pages/cart_page.dart';
import 'package:glasess_app/routes/AppRoutes.dart';
import 'package:loader_overlay/loader_overlay.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorHelper.whiteDarker,
      appBar: AppBar(
          elevation: 0,
          backgroundColor: ColorHelper.whiteDarker,
          leading: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.05,
                  top: Get.width * 0.01,
                  bottom: Get.width * 0.01),
              child: Container(
                decoration: BoxDecoration(
                    color: ColorHelper.dark,
                    borderRadius: BorderRadius.circular(10)),
                child: Icon(Icons.navigate_before_sharp,
                    color: ColorHelper.white, size: 35),
              ),
            ),
          ),
          leadingWidth: Get.width * 0.17,
          centerTitle: true,
          title: const Text('Cart',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600))),
      body: const CartPage(),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(
            left: Get.width * 0.05,
            right: Get.width * 0.05,
            top: Get.height * 0.01),
        height: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                context.loaderOverlay.show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(seconds: 1), () {
                  Get.toNamed(AppRoutes.ORDER_COMPLETE);
                  context.loaderOverlay.hide();
                });
              },
              child: Container(
                width: Get.width * 0.90,
                height: Get.height * 0.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.dark),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Checkout',
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
