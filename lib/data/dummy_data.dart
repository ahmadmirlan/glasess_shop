import 'package:flutter/cupertino.dart';
import 'package:glasess_app/models/category_model.dart';
import 'package:glasess_app/models/item_model.dart';

class DummyData {
  static List<ItemModel> items = [
    ItemModel(0, 'Oakley Admission', 'glass_2.png', categories[1], 163, 4.9, 112,
        const Color.fromRGBO(235, 244, 243, 1)),
    ItemModel(1, 'Oakley Reedmace', 'glass_4.png', categories[1], 251, 4.6, 34,
        const Color.fromRGBO(244, 235, 236, 1)),
    ItemModel(2, 'Oakley Admission (56) Satin Black', 'glass_1.png',
        categories[1], 253, 4.5, 80, const Color.fromRGBO(233, 234, 238, 1)),
    ItemModel(3, 'Oakley Socket TI (56) Pewter', 'glass_3.png', categories[1],
        200, 4.8, 10, const Color.fromRGBO(235, 244, 243, 1)),
    ItemModel(4, 'Oakley Metal Plate TI Satin Black', 'glass_5.png',
        categories[1], 200, 4.6, 121, const Color.fromRGBO(234, 233, 239, 1)),
    ItemModel(4, 'Ray-Ban RB1536F 3730 s48', 'glass_6.png',
        categories[3], 240, 4.6, 121, const Color.fromRGBO(255,251,223,1)),
    ItemModel(4, 'Oakley Cartwheel (51) Pol Ice Blue', 'glass_7.png',
        categories[3], 220, 4.6, 121, const Color.fromRGBO(255,223,227,1)),
    ItemModel(4, 'Ray-Ban RB1900F 3833 s49', 'glass_8.png',
        categories[3], 199, 4.6, 121, const Color.fromRGBO(235,223,255,1)),
  ];

  static List<CategoryModel> categories = [
    CategoryModel(0, 'Show All'),
    CategoryModel(1, 'Men'),
    CategoryModel(2, 'Women'),
    CategoryModel(3, 'Kids Collection'),
  ];

  static String description =
      'Focused on bringing customers the latest trends and cutting edge products, we are a one stop shop for the best brands in the industry. We are committed to providing customers with a great shopping experience and the brand loyalty they crave. Focusing on the very latest in affordable fashion styles, both attire and stunning accessories, we feature thousands of the newest product lines, providing maximum choice and convenience to our discerning clientele. We also aim to provide an extensive range of high quality, trendy fashion clothing together with a professional dedicated service to our valued customers from all over the world.';
}
