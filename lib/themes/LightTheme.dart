import 'package:flutter/material.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: ColorHelper.white,
    highlightColor: Colors.transparent,
    splashColor: Colors.transparent,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.latoTextTheme().copyWith(
    ),
);
