import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/models/category_model.dart';

class FilterWidget extends StatelessWidget {
  final CategoryModel category;
  final bool isSelected;

  const FilterWidget(
      {Key? key, required this.category, required this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(category.name,
            style: TextStyle(
                fontSize: 20,
                color: isSelected
                    ? ColorHelper.dark
                    : ColorHelper.dark.withOpacity(0.6),
                fontWeight: isSelected ? FontWeight.w700 : FontWeight.w400)),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: isSelected ? ColorHelper.dark : ColorHelper.white,
              shape: BoxShape.circle),
        )
      ],
    );
  }
}
