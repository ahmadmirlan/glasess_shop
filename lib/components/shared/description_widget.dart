import 'package:flutter/cupertino.dart';
import 'package:glasess_app/helper/color_helper.dart';

class DescriptionWidget extends StatefulWidget {
  final String text;

  const DescriptionWidget({Key? key, required this.text}) : super(key: key);

  @override
  _DescriptionWidget createState() => _DescriptionWidget();
}

class _DescriptionWidget extends State<DescriptionWidget> {
  late bool isExpanded;

  @override
  void initState() {
    super.initState();
    isExpanded = false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          setState(() {
            isExpanded = !isExpanded;
          });
        },
        child: Text(widget.text,
            overflow: TextOverflow.fade,
            maxLines: isExpanded ? 100 : 3,
            textAlign: TextAlign.justify,
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: ColorHelper.dark.withOpacity(0.6))));
  }
}
