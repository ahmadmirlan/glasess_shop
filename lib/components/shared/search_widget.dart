import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/helper/color_helper.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 60,
            width: Get.width * 0.7,
            child: TextField(
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                      left: 15, right: 15, top: 20, bottom: 20),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide:
                      BorderSide(color: ColorHelper.grey, width: 0.5)),
                  filled: true,
                  fillColor: ColorHelper.grey,
                  hintText: 'Search Glasses',
                  hintStyle: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: ColorHelper.dark)),
            ),
          ),
          Container(
            height: 55,
            width: Get.width * 0.16,
            decoration: BoxDecoration(
                color: ColorHelper.dark,
                borderRadius: BorderRadius.circular(10)),
            child: Icon(Icons.search, color: ColorHelper.white),
          )
        ],
      ),
    );
  }
}
