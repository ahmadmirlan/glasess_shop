import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/controller/cart_controller.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/models/order_item_model.dart';

class CartItemCardWidget extends StatelessWidget {
  final OrderedItemModel orderedItem;

  const CartItemCardWidget({Key? key, required this.orderedItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<CartController>();
    return Container(
      padding: EdgeInsets.all(Get.width * 0.03),
      decoration: BoxDecoration(
          color: ColorHelper.dark.withOpacity(0.1), borderRadius: BorderRadius.circular(15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: Get.width * 0.4,
            child: Image.asset('assets/images/${orderedItem.item.icon}',
                fit: BoxFit.cover),
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(orderedItem.item.name,
                  style: const TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 14)),
              Container(
                margin: EdgeInsets.only(
                    top: Get.height * 0.01, bottom: Get.height * 0.01),
                child: Text('\$${orderedItem.item.price.toStringAsFixed(2)}',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: ColorHelper.dark.withOpacity(0.6))),
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      controller.removeItemQuantity(orderedItem.item);
                    },
                    child: Container(
                      padding: const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ColorHelper.dark.withOpacity(0.4)),
                      child: Icon(Icons.remove, color: ColorHelper.white),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(3),
                    margin: EdgeInsets.only(
                        left: Get.width * 0.02, right: Get.width * 0.02),
                    child: Text('${orderedItem.quantity}',
                        style:
                            TextStyle(color: ColorHelper.dark, fontSize: 22)),
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.addItemQuantity(orderedItem.item);
                    },
                    child: Container(
                      padding: const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: ColorHelper.dark),
                      child: Icon(Icons.add, color: ColorHelper.white),
                    ),
                  ),
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
