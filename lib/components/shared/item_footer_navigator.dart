import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/components/shared/default_loading_widget.dart';
import 'package:glasess_app/controller/cart_controller.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/models/item_model.dart';
import 'package:glasess_app/routes/AppRoutes.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ItemFooterNavigator extends StatelessWidget {
  final ItemModel item;

  const ItemFooterNavigator({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(builder: (controller) {
      return Container(
        height: Get.height * 0.15,
        padding:
            EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
        decoration: BoxDecoration(color: ColorHelper.white),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                context.loaderOverlay
                    .show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(milliseconds: 500), () {
                  controller.addItemToCart(item);
                  final snackBar = SnackBar(
                    backgroundColor: ColorHelper.dark,
                    content: Text('${item.name} has been added to the cart!',
                        style: GoogleFonts.poppins(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w500)),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  context.loaderOverlay.hide();
                });
              },
              child: Container(
                height: Get.height * 0.07,
                width: Get.height * 0.07,
                decoration: BoxDecoration(
                    color: ColorHelper.dark.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(10)),
                child: const Icon(Icons.shopping_cart_outlined, size: 35),
              ),
            ),
            GestureDetector(
              onTap: () {
                context.loaderOverlay
                    .show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(milliseconds: 500), () {
                  controller.addItemToCart(item);
                  Get.toNamed(AppRoutes.CART);
                  context.loaderOverlay.hide();
                });
              },
              child: Container(
                width: Get.width * 0.70,
                height: Get.height * 0.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.dark),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Buy Now',
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
