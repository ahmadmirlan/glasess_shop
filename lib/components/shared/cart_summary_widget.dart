import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/helper/count_total_helper.dart';
import 'package:glasess_app/models/order_item_model.dart';

class CartSummaryWidget extends StatelessWidget {
  final List<OrderedItemModel> orderedItem;

  const CartSummaryWidget({Key? key, required this.orderedItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final subTotal = CountTotalHelper.countSubTotal(orderedItem);
    final ppn = CountTotalHelper.countPPN(subTotal);
    return Container(
      padding: EdgeInsets.all(Get.width * 0.05),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: ColorHelper.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.02),
            child: const Text('Amount',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Item Total',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
              Text('\$${subTotal.toStringAsFixed(2)}',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: ColorHelper.dark.withOpacity(0.6),
                      fontSize: 16)),
            ],
          ),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.01, bottom: Get.height * 0.01),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('PPN 11%',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
              Text('\$${ppn.toStringAsFixed(2)}',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: ColorHelper.dark.withOpacity(0.6),
                      fontSize: 16)),
            ],
          ),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.02, bottom: Get.height * 0.02),
            decoration: const BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, style: BorderStyle.solid))),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Grand Total',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22)),
              Text('\$${(subTotal + ppn).toStringAsFixed(2)}',
                  style: const TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 22)),
            ],
          ),
        ],
      ),
    );
  }
}
