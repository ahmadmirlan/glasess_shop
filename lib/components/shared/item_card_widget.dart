import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/models/item_model.dart';

class ItemCardWidget extends StatelessWidget {
  final ItemModel item;

  const ItemCardWidget({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: ColorHelper.white, borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15), color: item.color),
            height: Get.height * 0.15,
            child: Image.asset('assets/images/${item.icon}'),
          ),
          Expanded(
              child: Container(
            padding: EdgeInsets.only(
                left: Get.width * 0.03,
                right: Get.width * 0.03),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(item.name,
                    style: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.w600)),
                Text('\$${item.price.toStringAsFixed(2)}', style: const TextStyle(
                  fontSize: 16
                )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.star_outlined, color: ColorHelper.yellow),
                        Container(
                          margin: EdgeInsets.only(left: Get.width * 0.01),
                        ),
                        Text('${item.ratings}',
                            style: const TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 16))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.rate_review, color: ColorHelper.dark.withOpacity(0.3)),
                        Container(
                          margin: EdgeInsets.only(left: Get.width * 0.01),
                        ),
                        Text('(${item.reviewed})',
                            style: const TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 15))
                      ],
                    ),
                  ],
                )
              ],
            ),
          ))
        ],
      ),
    );
  }
}
