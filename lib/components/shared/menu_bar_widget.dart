import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:glasess_app/controller/cart_controller.dart';
import 'package:glasess_app/helper/color_helper.dart';
import 'package:glasess_app/routes/AppRoutes.dart';

class MenuBarWidget extends StatelessWidget {
  const MenuBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(builder: (controller) {
      return Container(
        padding:
            EdgeInsets.only(left: Get.width * 0.06, right: Get.width * 0.06),
        decoration: BoxDecoration(
            color: ColorHelper.white,
            border: Border(
                top: BorderSide(
                    color: ColorHelper.dark.withOpacity(0.3), width: 0.2))),
        height: 90,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.home_rounded,
                  color: ColorHelper.dark,
                  size: 30,
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.bookmark_outline,
                  color: ColorHelper.dark.withOpacity(0.5),
                  size: 30,
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.toNamed(AppRoutes.CART);
                  },
                  child: Badge(
                    badgeColor: Colors.redAccent,
                    badgeContent: Text('${controller.cartList.length}',
                        style: TextStyle(color: ColorHelper.white)),
                    child: Icon(
                      Icons.local_grocery_store_outlined,
                      color: ColorHelper.dark.withOpacity(0.5),
                      size: 30,
                    ),
                  ),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.settings,
                  color: ColorHelper.dark.withOpacity(0.5),
                  size: 30,
                )
              ],
            ),
          ],
        ),
      );
    });
  }
}
